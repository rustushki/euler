#include <iostream>
#include <vector>
#include <fstream>

#define MAX 9

/**
 * SOLVED - all cells are non-zero, and the rows, columns and groups contain no duplicates
 * INVALID - all cells are non-zero, but at least one row, column or group contains a duplicate
 * UNSOLVED - at least one cell has a value of 0
 */
enum SolutionStatus {
    SOLVED,
    INVALID,
    UNSOLVED
};

/**
 * A logical representation of a Sudoku grid.
 * Spots - the cells of the grid. 0 means unfilled
 * Could Be - the remaining possible values of the cell that have not been ruled out
 */
struct Puzzle {
    std::vector< std::vector<int> > spots;
    std::vector< std::vector< std::vector<int> > > couldBe;
};

/**
 * Initialize the puzzle by populating each cell's initial "could be" values.
 * @param puzzle
 */
void initialize(Puzzle& puzzle) {
    for (int y = 0; y < MAX; y++) {
        puzzle.couldBe.emplace_back(std::vector< std::vector<int> >());
        for (int x = 0; x < MAX; x++) {
            puzzle.couldBe[y].emplace_back(std::vector<int>({1, 2, 3, 4, 5, 6, 7, 8, 9}));
        }
    }
}

/**
 * Analyze the provided puzzle and determine if it is SOLVED, UNSOLVED or INVALID.
 * @param puzzle
 * @return status
 */
SolutionStatus getSolutionStatus(Puzzle& puzzle) {
    for (int y = 0; y < MAX; y++) {
        for (int x = 0; x < MAX; x++) {
            if (puzzle.spots[y][x] == 0) {
                if (puzzle.couldBe[y][x].size() == 0) {
                    return INVALID;
                }
                return UNSOLVED;
            }
        }
    }

    return SOLVED;
}

/**
 * Eliminate the provided "could be" value for the given cell (denoted by x & y) for the puzzle.
 * @param puzzle
 * @param x
 * @param y
 * @param couldBe
 */
void removeCouldBe(Puzzle& puzzle, int x, int y, int couldBe) {
    for (int n = 0; n < puzzle.couldBe[y][x].size(); n++) {
        if (puzzle.couldBe[y][x][n] == couldBe) {
            puzzle.couldBe[y][x].erase(puzzle.couldBe[y][x].begin() + n);
            break;
        }
    }
}

/**
 * Determine if the "could be" value is present in the provided row of the puzzle.
 * @param puzzle
 * @param y
 * @param couldBe
 * @return true if it is
 */
bool checkRow(Puzzle& puzzle, int y, int couldBe) {
    bool found = false;
    for (int x = 0; x < MAX; x++) {
        if (puzzle.spots[y][x] == couldBe) {
            found = true;
            break;
        }
    }
    return found;
}

/**
 * Determine if the "could be" value is present in the provided column of the puzzle.
 * @param puzzle
 * @param x
 * @param couldBe
 * @return true if it is
 */
bool checkColumn(Puzzle& puzzle, int x, int couldBe) {
    bool found = false;
    for (int y = 0; y < MAX; y++) {
        if (puzzle.spots[y][x] == couldBe) {
            found = true;
            break;
        }
    }
    return found;
}

/**
 * Determine if the "could be" value is present in the provided group of the puzzle.
 * @param puzzle
 * @param groupX 0, 1 or 2
 * @param groupY 0, 1 or 2
 * @param couldBe
 * @return true if it is
 */
bool checkGroup(Puzzle& puzzle, int groupX, int groupY, int couldBe) {
    bool found = false;
    for (int y = groupY * 3; y < groupY * 3 + 3; y++) {
        for (int x = groupX * 3; x < groupX * 3 + 3; x++) {
            if (puzzle.spots[y][x] == couldBe) {
                found = true;
                break;
            }
        }
    }
    return found;
}

/**
 * Fill in the given cell with a new value (clearing the "could be" values for that cell).
 * @param puzzle
 * @param x
 * @param y
 * @param newValue
 */
void completeSpot(Puzzle& puzzle, int x, int y, int newValue) {
    puzzle.spots[y][x] = newValue;
    puzzle.couldBe[y][x].clear();
}


/**
 * Iterate through each "could be" value for a cell and attempt to logically determine that value is the answer for the
 * cell, if it can be ruled out as not an answer, or if it is still a possiblity.
 * <p>
 * @param puzzle
 * @param x
 * @param y
 * @return true if any cell was filled or if the "could be" values were altered
 */
bool tryToFillSpot(Puzzle& puzzle, int x, int y) {
    bool anyChanges = false;

    if (puzzle.spots[y][x] == 0) {
        for (auto iter = puzzle.couldBe[y][x].begin(); iter < puzzle.couldBe[y][x].end();) {
            if (checkRow(puzzle, y, *iter)) {
                removeCouldBe(puzzle, x, y, *iter);
                anyChanges = true;
            } else if (checkColumn(puzzle, x, *iter)) {
                removeCouldBe(puzzle, x, y, *iter);
                anyChanges = true;
            } else if (checkGroup(puzzle, x / 3, y / 3, *iter)){
                removeCouldBe(puzzle, x, y, *iter);
                anyChanges = true;
            } else {
                iter++;
            }
        }

        if (puzzle.couldBe[y][x].size() == 1) {
            completeSpot(puzzle, x, y, puzzle.couldBe[y][x][0]);
            anyChanges = true;
        }
    }

    return anyChanges;
}

/**
 * Prints the puzzle grid. Here for debugging purposes.
 * @param puzzle
 */
void print(Puzzle& puzzle) {
    for (int y = 0; y < MAX; y++) {
        for (int x = 0; x < MAX; x++) {
            std::cout << puzzle.spots[y][x];
        }

        std::cout << std::endl;
    }
    std::cout << std::endl;
}

/**
 * Read the puzzles from Problem 96's input file and generates a vector of Puzzle objects.
 * @return
 */
std::vector<Puzzle> readPuzzles() {
    std::vector<Puzzle> puzzles;

    std::ifstream infile("p096_sudoku.txt");

    // Discard the "Grid ..." marker
    std::string line;
    while (std::getline(infile, line)) {
        Puzzle puzzle;

        initialize(puzzle);
        int row = 0;
        while (row < MAX) {
            std::getline(infile, line);
            row++;

            std::vector<int> row;
            for (char charDigit : line) {
                row.emplace_back(charDigit - 0x30);
            }
            puzzle.spots.emplace_back(row);
        }
        puzzles.emplace_back(puzzle);
    }

    return puzzles;
}

/**
 * Try to fill each cell in the grid until it is not possible to make any further logical deductions.
 * <p>
 * After this algorithm concludes, the puzzle will either be solved OR it will be in a state where a guess must be made
 * in order to continue. This can be discerned based on the return value.
 * @param puzzle
 * @return
 */
SolutionStatus logicalDeduction(Puzzle& puzzle) {
    bool anyChanges = true;
    while (anyChanges) {
        anyChanges = false;
        for (int x = 0; x < MAX; x++) {
            for (int y = 0; y < MAX; y++) {
                anyChanges |= tryToFillSpot(puzzle, x, y);
            }
        }
    }

    return getSolutionStatus(puzzle);
}

/**
 * Make a deep copy of the provided puzzle.
 * @param puzzle
 * @return copy
 */
Puzzle copyPuzzle(Puzzle& puzzle) {
    Puzzle copy;

    // Copy solved slots
    for (std::vector<int> originalRow : puzzle.spots) {
        std::vector<int> copyRow;
        for (int slotValue : originalRow) {
            copyRow.push_back(slotValue);
        }
        copy.spots.push_back(copyRow);
    }

    // Copy "could be" guesses
    for (std::vector< std::vector<int> > originalCouldBeRow : puzzle.couldBe) {
        std::vector< std::vector<int> > copyCouldBeRow;
        for (std::vector<int> originalCouldBeValues : originalCouldBeRow) {
            std::vector<int> copyCouldBeValues;
            for (int couldBeValue : originalCouldBeValues) {
                copyCouldBeValues.push_back(couldBeValue);
            }
            copyCouldBeRow.push_back(copyCouldBeValues);
        }
        copy.couldBe.push_back(copyCouldBeRow);
    }
    return copy;
}

/**
 * Find the first unsolved cell that has the least number of "could be" values (i.e. the least risky guess) and return
 * a list of "guess puzzles" which each represent the filling in of one of those "could be" values.
 * @param puzzle
 * @return list of guess puzzles
 */
std::vector<Puzzle> forkPuzzleOnLeastRiskyGuess(Puzzle& puzzle) {
    int leastRiskyX = 0;
    int leastRiskyY = 0;
    int leastRiskyGuessCount = MAX;

    for (int y = 0; y < MAX; y++) {
        for (int x = 0; x < MAX; x++) {
            if (!puzzle.couldBe[y][x].empty() && puzzle.couldBe[y][x].size() < leastRiskyGuessCount) {
                leastRiskyGuessCount = puzzle.couldBe[y][x].size();
                leastRiskyX = x;
                leastRiskyY = y;

                if (leastRiskyGuessCount == 2) {
                    break;
                }
            }
        }

        if (leastRiskyGuessCount == 2) {
            break;
        }
    }

    std::vector<Puzzle> guesses;
    for (int guessValue : puzzle.couldBe[leastRiskyY][leastRiskyX]) {
        Puzzle guess = copyPuzzle(puzzle);
        completeSpot(guess, leastRiskyX, leastRiskyY, guessValue);
        guesses.emplace_back(guess);
    }

    return guesses;
}

/**
 * Overwrite the values of the destination puzzle with the source puzzle's values.
 * @param source
 * @param destination
 */
void replaceInto(Puzzle& source, Puzzle& destination) {
    for (int y = 0; y < MAX; y++) {
        for (int x = 0; x < MAX; x++) {
            destination.spots[y][x] = source.spots[y][x];
            destination.couldBe[y][x].clear();
            destination.couldBe[y][x].insert(destination.couldBe[y][x].end(), source.couldBe[y][x].begin(), source.couldBe[y][x].end());

        }
    }

}

/**
 * Assume the puzzle has a valid solution, solve it.
 * <p>
 * First try to solve it using logical deduction. If that is not enough, make least risky guess puzzles and attempt to
 * solve those until a solution is found.
 * @param puzzle
 * @return
 */
SolutionStatus solve(Puzzle& puzzle) {
    // Try to solve the puzzle through logical deduction
    if (logicalDeduction(puzzle) == UNSOLVED) {
        std::vector<Puzzle> guesses = forkPuzzleOnLeastRiskyGuess(puzzle);

        for (Puzzle guess : guesses) {
            SolutionStatus status = solve(guess);
            if (status == SOLVED) {
                replaceInto(guess, puzzle);
                return SOLVED;

            } else if (status == INVALID) {
                continue;
            }
        }

        return INVALID;
    }

    return getSolutionStatus(puzzle);
}

/**
 * Sum the first three horizontal digits (as a three digit number) of the upper left group of the provided puzzle grids.
 * <p>
 * This is the required format of the answer to Problem 96.
 * @param puzzles
 * @return
 */
long sumAnswer(std::vector<Puzzle> puzzles) {
    long answer = 0;
    for (Puzzle puzzle : puzzles) {
        answer += puzzle.spots[0][0] * 100;
        answer += puzzle.spots[0][1] * 10;
        answer += puzzle.spots[0][2] * 1;
    }

    return answer;
}

/**
 * Read the puzzle from the input text file, solve them, then output the answer to Problem 96.
 * @return
 */
int main() {
    std::vector<Puzzle> puzzles = readPuzzles();
    for (Puzzle& puzzle : puzzles) {
        solve(puzzle);
    }
    std::cout << sumAnswer(puzzles) << std::endl;
    return 0;
}

