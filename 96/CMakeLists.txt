cmake_minimum_required(VERSION 3.10)
project(96)

set(CMAKE_CXX_STANDARD 11)

add_executable(96 main.cpp)